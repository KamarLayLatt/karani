<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//testing
Route::get('/backend2', function(){
	return view('layouts.backend2');
});
Route::get('/login2', function(){
	return view('auth.test_login');
});

//Backend
Auth::routes();
Route::get('admin-login', 'Auth\AdminLoginController@showLoginForm')->name('admin-login');
Route::post('admin-login', ['as'=>'admin-login','uses'=>'Auth\AdminLoginController@login']);
Route::get('admin-logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

Route::prefix('backend')->middleware('auth:admin')->namespace('backend')->group(function(){
    Route::get('/', 'AdminController@dashboard')->name('backend');
    Route::resource('admins', 'AdminController');
    Route::resource('categories', 'CategoryController');
    Route::resource('songs', 'SongController');
    Route::post('songs/search', 'SongController@search')->name('songs.search');
});

Route::get('/test','backend\SongController@test');

//frontend
Route::get('/', 'HomeController@index')->name('home');

//Application
// Route::get('{any}', 'AppController@index')->where('any', '[\/\w\.-]*');

//facebook login
Route::post('/sociallogin/{provider}', 'api\RegisterController@SocialSignup');
Route::get('/app/auth/{provider}/callback', 'api\RegisterController@callBack')->where('provider', '.*');


