<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function(){
    Route::get('/app/user/my-profile', 'api\UserController@profile');
    Route::get('/app/logout', 'api\RegisterController@logout');
    Route::get('/app/my-song','api\SongController@mySong');
    Route::get('/app/song/{id}/check', 'api\SongController@checkUserSong');
    Route::post('/app/song/store', 'api\SongController@store');
    Route::post('/app/song/{id}/update', 'api\SongController@update');
    Route::get('/app/song/edit/{id}', 'api\SongController@edit');
    Route::get('/app/song/delete/{id}', 'api\SongController@delete');
    Route::get('/app/favorite/add/{id}', 'api\FavoriteController@add');
    Route::get('/app/favorite/remove/{id}', 'api\FavoriteController@remove');
    Route::get('/app/favorite/my-fav', 'api\FavoriteController@myFav');
    Route::get('/app/favorite/check/{id}', 'api\FavoriteController@hasFavorite');
});

Route::post('/app/register','api\RegisterController@register');
Route::post('/app/login','api\RegisterController@login');
Route::get('/app/signup/activate/{token}', 'api\RegisterController@signupActivate');

Route::get('/app/song/list','api\SongController@index');
Route::get('/app/song/category/{id}/list', 'api\SongController@categoryList');
Route::get('/app/song/show/{id}', 'api\SongController@show');
Route::get('/app/category/list', 'api\CategoryController@index');
Route::post('/app/song/search', 'api\SongController@search');

Route::get('/testapp',function(){
    return 'hello';
});

