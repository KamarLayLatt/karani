<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Application routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Application')->group(function(){
    Route::get('category-list', 'CategoryController@categoryList');
    Route::get('song-list', 'SongController@songList');
    Route::get('song/detail','SongController@songDetail');
    Route::get('category/song-list','SongController@categorySongList');
    Route::get('song/search','SongController@searchSong');
    Route::get('song/top-song-list', 'SongController@topSongList');


});