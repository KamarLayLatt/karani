<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('user_type', 20);
            $table->unsignedBigInteger('song_category_id');
            $table->string('title', 50);
            $table->string('composer', 30)->nullable();
            $table->string('key', 10)->nullable();
            $table->string('style', 20)->nullable();            
            $table->text('chord')->nullable();
            $table->text('mp3')->nullable();
            $table->text('description')->nullable();
            $table->text('content');
            $table->foreign('song_category_id')->references('id')->on('song_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
}
