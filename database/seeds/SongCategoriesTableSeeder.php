<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;


class SongCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,5) as $index)
        {
            DB::table('song_categories')->insert([
                'title' => $faker->word,
                'description' => $faker->text($maxNbChars = 50),
                'file' => $faker->word,
            ]);
        }
    }
}
