<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class SongsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,20) as $index)
        {
            DB::table('songs')->insert([
                'user_id' => $faker->numberBetween(1,20),
                'user_type' => $faker->word,
                'song_category_id' => $faker->numberBetween(1,5),
                'title' => $faker->word,
                'composer' => $faker->name,
                'key' => Str::random(1),
                'style' => Str::random(5),
                'content' => $faker->paragraph,
                'description' => $faker->text($maxNbChars = 50),
                'mp3' => Str::random(10),
            ]);
        }
    }
}
