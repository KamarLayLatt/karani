<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Kyawnaingtun\Tounicode\TounicodeTrait;

class Song extends Model
{
    // use TounicodeTrait;
    
    protected $fillable = [
        'uniquid','user_id', 'user_type', 'song_category_id',
        'title','composer','singer','key','style','youtube_id', 'description','content','chord',
        'public_status'
    ];

    // protected $convertable = ['title','composer','singer','key','style','mp3','description','content','chord'];

    public function song_category()
    {
        return $this->belongsTo('App\SongCategory');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function admin()
    {
        return $this->belongsTo('App\Admin', 'user_id');
    }

    public function song_views()
    {
        return $this->hasMany('App\SongView');
    }
}
