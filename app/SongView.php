<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongView extends Model
{
    protected $fillable = [
        'song_id',
    ];
}
