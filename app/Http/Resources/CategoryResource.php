<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(is_file(Storage::path('public/uploads/category/'.$this->file)))
        {
            $image_url = url(Storage::url('public/uploads/category/'.$this->file));
        }
        else{
            $image_url = asset('photos/default.png');
        }
        return [
            'categoryId' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $image_url,
            'total_songs' => count($this->songs),
        ];
    }
}
