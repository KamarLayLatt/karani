<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\SongCategory;

class SongListResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $song_category = new CategoryResource(SongCategory::find($request->songCategoryId));
        return [
            'responseCode' => 1,
            'responseMessage' => 'Successfully retrieve Song List',
            'song_category' => $song_category,
            'data' => $this->collection,
        ];
    }
}
