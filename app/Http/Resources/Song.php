<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class Song extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
    
        $user= User::find($this->user_id);
        if($this->user_type == 'user')
        {
            $user_name = $user->name;
        }
        else{
            $user_name = 'Admin';
        }
       return[
           'id' => $this->id,
           'uniquid' => $this->uniquid,
           'user_name' => $user_name,
           'title' => $this->title,
           'key' => $this->key,
           'style' => $this->style,
           'category_id' => $this->song_category_id,
           'content' => $this->content,
           'composer' => $this->composer,
           'singer' => $this->singer,
           'youtube_id' => $this->youtube_id,
           'description'=> $this->description,
           'public_status' => $this->public_status,
           'chord' => $this->chord,
       ];
    }
}
