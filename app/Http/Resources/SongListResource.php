<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class SongListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user= User::find($this->user_id);
        if($this->user_type == 'user')
        {
            $user_name = $user->name;
        }
        else{
            $user_name = 'Admin';
        }
        return [
            'songId' => $this->id,
            'uniquid' => $this->uniquid,
            'user_name' => $user_name,
            'title' => $this->title,
            'style' => $this->style,
            'content' => $this->content,
        ];
    }
}
