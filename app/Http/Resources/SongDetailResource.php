<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class SongDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user= User::find($this->user_id);
        if($this->user_type == 'user')
        {
            $user_name = $user->name;
        }
        else{
            $user_name = 'Admin';
        }

        return [
            'songId' => $this->id,
            'uniquid' => $this->uniquid,
            'username' => $user_name,
            'user_type' => $this->user_type,
            'song_category' => $this->song_category->title,
            'title' => $this->title,
            'composer' => $this->composer,
            'singer' => $this->singer,
            'key' => $this->key,
            'style' => $this->style,
            'chord' => $this->chord,
            'youtube_id' => $this->youtube_id,
            'description' => $this->description,
            'public_status' => $this->public_status,
            'content' => $this->content,
        ];
    }
}
