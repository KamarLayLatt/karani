<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class Favorite extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user= User::find($this->user_id);
        if($this->user_type == 'user')
        {
            $user_name = $user->name;
        }
        else{
            $user_name = 'Admin';
        }
        return[
            'id' => $this->song->id,
            'title' => $this->song->title,
            'user_name' => $user_name,
            'key' => $this->song->key,
            'style' => $this->song->style,
            'content' => $this->song->content,
            'composer' => $this->song->composer,
            'singer' => $this->song->singer,
        ];
    }
}
