<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Notifications\SignupActivate;
use App\User;
use Socialite;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => 'validation_error',
                'errors' => $validator->errors(),
            ]);       
        }

        $password= Hash::make($request->get('password'));

        $activation_number = rand(100000,999999);

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $password,
            'activation_token' => str_random(60),
            'activation_number' => $activation_number,
        ]);

        $data = [
            'user' => $user,
            'activation_number' => $activation_number,
        ];

        $user->notify(new SignupActivate($data));

        $token =  $user->createToken('MyApp')->accessToken;

        return response()->json(['success'=> true, 'token' => $token, 'name' => $user->name]);
    }

    public function signupActivate($token)
    {
        $user = User::where('activation_number', $token)->where('active', false)->first();
        if (!$user) {
            $data = [
                'success' => false,
                'message' => 'This activation number is invalid.',
            ];

            return response()->json($data);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->save();
        $token =  $user->createToken('MyApp')->accessToken;
        $data = [
            'success' => true,
            'token' => $token,
        ];

        return response()->json($data);
    }

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json(['success'=>'validation_error', 'errors' => $validator->errors()]);       
        }
        //return response()->json($request->all());
        $user = User::where('email', $request->email)->first();
        
        $email = $request['email'];
        $password = $request['password'];

        if ($user) {
            if($user->active == 1)
            {
                if (Hash::check($password, $user->password)) {
                    $token =  $user->createToken('MyApp')->accessToken;
                    $user_name = $user->name;
                    return response()->json(['success'=> true,'token' => $token, 'name' => $user_name]);
                } else {
                    return response()->json(['success'=>'login_error', 'error' => 'password miss match']);
                }
            }
            else{
                return response()->json(['success'=>'active_error', 'error' => 'please confirm your gmail account']);
            }
            
        } else {
            return response()->json(['success'=>'null_error', 'error' => 'User does not exit']);
        }
    }

    public function logout(Request $request) {
        $request->user()->token()->revoke();
        return response()->json([
            'logout' => true,
        ]);
    }

    public function SocialSignup($provider)
    {
        // Socialite will pick response data automatic 
        $social_user = Socialite::driver($provider)->stateless()->user();

        $check_user = User::where('email', $social_user->user['email'])->first();
        

        if(empty($check_user))
        {
            $user = User::create([
            'name' => $social_user->user['name'],
            'email' => $social_user->user['email'],
            'provider' => $provider,
            'provider_id' => $social_user->user['id'],
            'active' => 1,
            ]);

            $token =  $user->createToken('MyApp')->accessToken;

            $data = [
                'user' => $user,
                'token' => $token,
            ];


            return response()->json($data);
        }
        else{
            $token =  $check_user->createToken('MyApp')->accessToken;
            $data = [
                'user' => $check_user,
                'token' => $token,
            ];

            
            return response()->json($data);
        }
        
    }

    public function callBack()
    {
        # code...
    }
}
