<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\FavoriteCollection;
use App\Favorite;
use App\User;
use Auth;
use App\Song;
class FavoriteController extends Controller
{
    public function myFav()
    {
        return new FavoriteCollection(Favorite::where('user_id', Auth::user()->id)->paginate(10));

        // $favorites = Favorite::where('user_id', Auth::user()->id)->get();
        // return response()->json($favorites);
    }
    public function add($id)
    {
        Favorite::Create([
            'song_id' => $id,
            'user_id' => Auth::user()->id,
        ]);

        return response()->json(['success' => true]);
    }

    public function remove($id)
    {
        $favorite = Favorite::where('song_id',$id)->where('user_id', Auth::user()->id)->first();

        if(!empty($favorite))
        {
            $favorite->delete();
            return response()->json(['success' => true]);
        }
        else{
            return response()->json(['success' => false, 'error' => 'song not exit']);
        }        
    }
    
    public function hasFavorite($id)
    {
        //$song = Song::find($id);

        $favorite = Favorite::where('song_id', $id)->where('user_id', Auth::user()->id)->first();

        if(!empty($favorite))
        {
            return response()->json(['select_fav' => true , 'id' => $id]);
        }
        else{
            return response()->json(['select_fav' => false, 'id' => $id]);
        }
    }
}
