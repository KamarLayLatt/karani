<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SongCategory;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = SongCategory::orderBy('id','asc')->get();
        return response()->json($categories);
    }
}
