<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class UserController extends Controller
{
    public function profile()
    {
        $user= User::find(Auth::user()->id);
        return response()->json($user);        
    }
}
