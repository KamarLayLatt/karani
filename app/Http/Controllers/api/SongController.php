<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\SongCollection;
use App\Http\Resources\Song as SongResource;    
use App\Song;
use App\Category;
use Auth;
use Validator;
use Carbon\Carbon;

class SongController extends Controller
{
    public function index()
    {
        return new SongCollection(Song::where('public_status', 0)->paginate(10));        
    }

    public function categoryList($id)
    {
        return new SongCollection(Song::where('song_category_id',$id)
                                        ->where('public_status', 0)
                                        ->paginate(10));
    }

    public function show($id)
    {
        //$song = Song::find($id);
        
        //return response()->json($song);
        return new SongResource(Song::find($id));
    }

    public function edit($id)
    {
        return new SongResource(Song::find($id));
    }

    public function search(Request $request)
    {
        return new SongCollection(Song::where('title','like', '%' .  $request['search_data'] . '%')
                                        ->orWhere('content','like', '%' .  $request['search_data'] . '%')
                                        ->orWhere('uniquid','like', '%' .  $request['search_data'] . '%')
                                        ->get());
    }

    public function mySong()
    {
        return new SongCollection(Song::where('user_id', Auth::user()->id)->where('user_type', 'user')->paginate(10));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:30',            
            'category' => 'required',
            'composer' => 'max:30',
            'singer' => 'max:30',
            'key' => 'max:3',
            'style' => 'max:15',
            'description' => 'max:2000',
            'lyric' => 'required|min:20',
        ]);

        if($validator->fails()){
            return response()->json(['success'=>'validation_error', 'errors' => $validator->errors()]);       
        }

        $song= Song::Create([
            'user_id' => Auth::user()->id,
            'user_type' => 'user',
            'title' => $request['title'],
            'song_category_id' => $request['category'],
            'composer' => $request['composer'],
            'singer' => $request['singer'],
            'key' => $request['key'],
            'style' => $request['style'],
            'youtube_id' => $request['youtube_id'],
            'chord' => $request['chord'],
            'description' => $request['description'],
            'content' => $request['lyric'],
            'public_status' => $request['public_status'],
        ]);

        //create uniquid for song
        $now = Carbon::now();
        $second = $now->second;
        $second_string = strval($second);
        $length = strlen($second_string);

        if($length == 1)
        {
            $second = '0'.$second;
        }

        $uniquid = $second.$song->id;

        $song->uniquid = $uniquid;
        $song->save();

        return response()->json(['success' => true]);
    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:30',            
            'category' => 'required',
            'composer' => 'max:30',
            'singer' => 'max:30',
            'key' => 'max:3',
            'style' => 'max:15',
            'description' => 'max:2000',
            'lyric' => 'required|min:20',
        ]);

        if($validator->fails()){
            return response()->json(['success'=>'validation_error', 'errors' => $validator->errors()]);       
        }

        $song = Song::find($id);

        $song->title = $request['title'];
        $song->song_category_id = $request['category'];
        $song->composer = $request['composer'];
        $song->singer = $request['singer'];
        $song->key = $request['key'];
        $song->style = $request['style'];
        $song->chord = $request['chord'];
        $song->youtube_id = $request['youtube_id'];
        $song->description = $request['description'];
        $song->content = $request['lyric'];
        $song->public_status = $request['public_status'];

        $song->save();

        return response()->json(['success' => true]);
    }

    public function checkUserSong($song_id)
    {
        $user = Auth::user();
        $song = Song::find($song_id);

        if($user->id == $song->user_id)
        {
            return response()->json(['check' => true]);
        }
        else{
            return response()->json(['check' => false]);
        }

    }

    public function delete($song_id)
    {
        $song = Song::find($song_id);
        $song->delete();

        return response()->json(['success'=> true]);
    }
}
