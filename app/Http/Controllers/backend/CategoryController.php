<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SongCategory;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = SongCategory::orderBy('id','asc')->paginate(10);
        return view('backend.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'file' => 'required|image',
        ])->validate();

        if($request->hasFile('file'))
        {
                $file = $request->file('file');
                $filename = uniqid().'_'.$file->getClientOriginalName();
                Storage::disk('public')->putFileAs('/uploads/category/', $file, $filename);
        }

        SongCategory::Create([
            'title' => $request['title'],
            'description' => $request['description'],
            'file' => $filename,
        ]);

        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = SongCategory::find($id);
        return view('backend.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = SongCategory::find($id);
        return view('backend.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
        ])->validate();       

        $category =  SongCategory::find($id);

        if($request->hasFile('file'))
        {
                $file = $request->file('file');
                $filename = uniqid().'_'.$file->getClientOriginalName();
                Storage::disk('public')->delete('/uploads/category/'. $category->file);
                Storage::disk('public')->putFileAs('/uploads/category/', $file, $filename);
                $category->file = $filename;
        }

        $category->title = $request['title'];
        $category->description = $request['description'];        
        $category->save();

        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = SongCategory::find($id);
        $songs = $category->songs;

        if($songs->count() == 0)
        {
            Storage::disk('public')->delete('/uploads/category/'. $category->file);
            $category->delete();
            return redirect(route('categories.index'));
        }
        else{
            return redirect()->back()->with('alert', 'Sorry,You can only delete when this category have no songs!');
        }


        
    }
}
