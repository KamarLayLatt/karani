<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Song;
use App\SongCategory;
use Validator;
use Carbon\Carbon;
use Auth;

class SongController extends Controller
{
    public function test()
    {
        $now = Carbon::now();
        $second = $now->second;
        $second_string = strval($second);
        $length = strlen($second_string);

        if($length == 1)
        {
            $second = '0'.$second;
        }

        return 'second=> '.$second.'<br>'.'count '.$length;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $songs = Song::paginate(10);
        $song = Song::find(3);

        return view('backend.songs.index', compact('songs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = SongCategory::all();
        return view('backend.songs.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'title' => 'required|max:255',
            'content' => 'required',
        ])->validate();

        $song = Song::Create([
            'user_id' => Auth::user()->id,
            'user_type' => 'admin',
            'title' => $request['title'],
            'song_category_id' => $request['song_category_id'],
            'composer' => $request['composer'],
            'singer' => $request['singer'],
            'key' => $request['key'],
            'style' => $request['style'],
            'youtube_id' => $request['youtube_id'],
            'chord' => $request['chord'],
            'description' => $request['description'],
            'content' => $request['content'],
        ]);


        //create uniquid for song
        $now = Carbon::now();
        $second = $now->second;
        $second_string = strval($second);
        $length = strlen($second_string);

        if($length == 1)
        {
            $second = '0'.$second;
        }

        $uniquid = $second.$song->id;

        $song->uniquid = $uniquid;
        $song->save();

        return redirect(route('songs.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $song = Song::find($id);
        return view('backend.songs.show', compact('song'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = SongCategory::all();
        $song = Song::find($id);
        return view('backend.songs.edit', compact('song','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $song = Song::find($id);
        Validator::make($request->all(), [
            'title' => 'required|max:255',
            'content' => 'required',
        ])->validate();

            $song->title = $request['title'];
            $song->song_category_id = $request['song_category_id'];
            $song->composer = $request['composer'];
            $song->singer = $request['singer'];
            $song->key = $request['key'];
            $song->style = $request['style'];
            $song->youtube_id = $request['youtube_id'];
            $song->chord = $request['chord'];
            $song->description = $request['description'];
            $song->content = $request['content'];

            $song->save();

        return redirect(route('songs.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $song = Song::find($id);
        $song->delete();

        return redirect(route('songs.index'));
    }

    public function search(Request $request)
    {
        $search = $request['search'];

        $songs = Song::where('title','like','%'.$search.'%')
                        ->orWhere('content','like','%'.$search.'%')
                        ->orWhere('uniquid','like','%'.$search.'%')
                        ->orwhereHas('song_category', function($query) use ($search){
                            $query->where('title', 'like', '%'.$search.'%');
                        })
                        ->paginate(10);

        return view('backend.songs.index', compact('songs'));
    }
}
