<?php

namespace App\Http\Controllers\Application;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class BaseController extends Controller
{
    public function successResponse($data, $message)
    {
        $response = [
            'responseCode' => 1,
            'responseMessage' => $message,
            'responseBody' => $data,
        ];

        return response()->json($response);
    }

    public function failResponse($errors)
    {
        $response = [
            'responseCode' => 1000,
            'responseMessage' => 'multiple errors',
            'error' => $errors,
        ];

        return response()->json($response);
    }

    public function validateResponse($request, $rules)
    {
    	$validator = Validator::make($request, $rules);

        if($validator->fails()){
            
            $errors_array = array(); 

            $errors = $validator->errors();

            foreach ($errors->all() as $error) {
                $error_array = explode(" ", $error);
                $data = [
                    "fieldCode" => $error_array[1],
                    "errorMessage" => $error,
                ];

                array_push($errors_array, $data);
            }

            return [
                'responseCode' => 1000,
                'responseMessage' => 'Multiple Error',
                'validate_status' => 1,
                'error' => $errors_array,

            ];  
            
        }else{
            return [
                'validate_status' => 0,
            ];  
        }
    }

    public function invalidResponse()
    {
        return [
            'responseCode' => 1001,
            'responseMessage' => "Invalid Sessions",
            'error' => [
                'responseCode' => '401',
                'responseMessage' => 'unauthorized user',
            ],
        ];
    }
}
