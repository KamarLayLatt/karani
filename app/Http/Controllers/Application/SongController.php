<?php

namespace App\Http\Controllers\Application;

use Illuminate\Http\Request;
use App\Http\Controllers\Application\BaseController;
use App\Http\Resources\SongListResourceCollection;
use App\Http\Resources\SongListResource;
use App\Http\Resources\SongDetailResource;
use App\Song;
use App\SongView;

class SongController extends BaseController
{
    public function songList()
    {
        return new SongListResourceCollection(Song::where('public_status', 0)
                                                    ->withCount('song_views')->orderBy('song_views_count','desc')
                                                    ->paginate(10));
    }

    public function songDetail(Request $request)
    {
        SongView::Create([
            'song_id' => $request->id,
        ]);
        $responseBody = new SongDetailResource(Song::find($request->id));
        return $this->successResponse($responseBody, 'Success retrieve song detail');
    }

    public function categorySongList(Request $request)
    {
        return new SongListResourceCollection(Song::where('public_status', 0)
                                            ->where('song_category_id', $request->songCategoryId)
                                            ->paginate(10));
    }

    public function searchSong(Request $request)
    {
        if($request->searchData != '')
        {
            return new SongListResourceCollection(Song::where('title','like', '%' .  $request['searchData'] . '%')
                                        ->orWhere('content','like', '%' .  $request['searchData'] . '%')
                                        ->orWhere('uniquid','like', '%' .  $request['searchData'] . '%')
                                        ->limit(30)
                                        ->get());
        }
        
    }

    public function topSongList()
    {
        $responseBody = [
            'topSongList' => SongListResource::collection(Song::where('public_status', 0)
                                                                    ->withCount('song_views')->orderBy('song_views_count','desc')
                                                                    ->limit(10)
                                                                    ->get()),
        ];

        return $this->successResponse($responseBody, 'Success retrieve Top Song list');
    }
}
