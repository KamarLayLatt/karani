<?php

namespace App\Http\Controllers\Application;

use Illuminate\Http\Request;
use App\Http\Controllers\Application\BaseController;
use App\Http\Resources\CategoryResource;
use App\SongCategory;
use App\Song;

class CategoryController extends BaseController
{
    public function categoryList()
    {

        $responseBody = [
            'totalSong' => count(Song::all()),
            'categoryList' => CategoryResource::collection(SongCategory::orderBy('id','asc')->get()),
        ];

        return $this->successResponse($responseBody, 'Success retrieve cateogory list');
    }
}
