<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $fillable = [
        'song_id','user_id'
    ];

    public function song()
    {
        return $this->belongsTo('App\Song');
    }
}
