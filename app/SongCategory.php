<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongCategory extends Model
{
    protected $fillable = [
        'title', 'description','file',
    ];

    public function songs()
    {
        return $this->hasMany('App\Song');
    }
}
