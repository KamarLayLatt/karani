<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;
use mysqli;

class CreateDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Database for this project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // Artisan::call('db:seed --class=CreateDatabaseSeeder');
        $servername = $this->ask('What is your host name? eg.127.0.0.1 ');
        $username = $this->ask('What is user name?');
        $password = $this->secret('What is password?');
        $database = $this->ask('What is your dadabase?');

        // Create connection
        $conn = new mysqli($servername, $username, $password);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 

        // Create database
        $sql = "CREATE DATABASE $database";
        if ($conn->query($sql) === TRUE) {
            echo "Database created successfully";
        } else {
            echo "Error creating database: " . $conn->error;
        }

        $conn->close();
    }
}
