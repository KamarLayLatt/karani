<!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" value="{{ csrf_token() }}" />
        <title>Song Application</title>
        <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
        <link rel='manifest' href="{{asset('mix-manifest.json')}}">

        {{-- <link rel="stylesheet" href='https://mmwebfonts.comquas.com/fonts/?font=mon3' />
        <link rel="stylesheet" href='https://mmwebfonts.comquas.com/fonts/?font=zawgyi' />
        <style>
            .unicode {
                font-family: "Pyidaungsu","MON3 Anonta 1" !important;
            }

            .zawgyi {
                font-family: "Zawgyi-One" !important;
            }
        </style> --}}
    </head>
    
    <body>
        <div id="app">
            <app></app>
        </div>
        {{-- <script src="{{ asset('js/bootstrap.js') }}"></script> --}}
        {{-- <script src="/dist/vue-social-sharing.min.js"></script> --}}
        <script src="{{ mix('js/app.js') }}"></script>


        {{-- pwa script --}}
        <script type="text/javascript">
            // This is the "Offline page" service worker

            // Add this below content to your HTML page, or add the js file to your page at the very top to register service worker

            // Check compatibility for the browser we're running this in
            if ("serviceWorker" in navigator) {
              if (navigator.serviceWorker.controller) {
                console.log("[PWA Builder] active service worker found, no need to register");
              } else {
                // Register the service worker
                navigator.serviceWorker
                  .register("pwabuilder-sw.js", {
                    scope: "./"
                  })
                  .then(function (reg) {
                    console.log("[PWA Builder] Service worker has been registered for scope: " + reg.scope);
                  });
              }
            }
        </script>
    </body>
    </html>