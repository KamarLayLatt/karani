@extends('layouts.backend')
@section('title','Category|View')

@section('head_title','Category')

@section('breadcrumbs')
    <li class="m-nav__item">
        <a href="{{route('categories.index')}}" class="m-nav__link">
            <span class="m-nav__link-text">Category</span>
        </a>
    </li>
    <li class="m-nav__separator">-</li>
    <li class="m-nav__item">
        <a href="{{route('categories.show', $category->id)}}" class="m-nav__link">
            <span class="m-nav__link-text">Show</span>
        </a>
    </li>   
@endsection

@section('content')

<div class="container col-md-8">
    <div class="card">
        <div class="card-title text-center mt-4 h3">Category Detail</div>
        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th> Title </th>
                    <td> {{$category->title}} </td>
                </tr>
                <tr>
                    <th> Description </th>
                    <td> {{$category->description}} </td>
                </tr>
                <tr>
                    <th> Photo </th>
                    <td><img style="height: 200px; width: 250px;" src="{{ asset('storage/uploads/category/'.$category->file) }}" /></td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection