@extends('layouts.backend')
@section('title','Song|View')


@section('head_title','Song')

@section('breadcrumbs')
    <li class="m-nav__item">
        <a href="{{route('songs.index')}}" class="m-nav__link">
            <span class="m-nav__link-text">Song</span>
        </a>
    </li>
    <li class="m-nav__separator">-</li>
    <li class="m-nav__item">
        <a href="{{route('songs.show', $song->id)}}" class="m-nav__link">
            <span class="m-nav__link-text">Show</span>
        </a>
    </li>   
@endsection



@section('content')
<div class="container col-md-8">
    <div class="card">
        <div class="card-title text-center mt-4 h3">Song Detail</div>
        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th> Title </th>
                    <td> {{$song->title}} </td>
                </tr>
                <tr>
                    <th> User </th>
                    @if($song->user_type == 'admin')
                        <td> {{$song->admin->name}} </td>
                    @else
                        <td> {{$song->user->name}} </td>
                    @endif
                </tr>
                <tr>
                    <th> User Type </th>
                    <td> {{$song->user_type}} </td>
                </tr>
                <tr>
                    <th> Category </th>
                    <td> {{$song->song_category->title}} </td>
                </tr>
                <tr>
                    <th> Composer </th>
                    <td> {{$song->composer}} </td>
                </tr>
                <tr>
                    <th> Singer </th>
                    <td> {{$song->singer}} </td>
                </tr>
                <tr>
                    <th> Key </th>
                    <td> {{$song->key}} </td>
                </tr>
                <tr>
                    <th> Style </th>
                    <td> {{$song->style}} </td>
                </tr>
                <tr>
                    <th> Mp3 </th>
                    <td> {{$song->mp3}} </td>
                </tr>
                <tr>
                    <th> Description </th>
                    <td> {{$song->description}} </td>
                </tr>
                <tr>
                    <th> Content </th>
                    <td> {!!$song->content!!} </td>
                </tr>
                <tr>
                    <th> Chord </th>
                    <td> {!!$song->chord!!} </td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection