@extends('layouts.backend')
@section('title','Song|Edit')

@section('head_title','Song')

@section('breadcrumbs')
    <li class="m-nav__item">
        <a href="{{route('songs.index')}}" class="m-nav__link">
            <span class="m-nav__link-text">Song</span>
        </a>
    </li>
    <li class="m-nav__separator">-</li>
    <li class="m-nav__item">
        <a href="{{route('songs.edit', $song->id)}}" class="m-nav__link">
            <span class="m-nav__link-text">Edit</span>
        </a>
    </li>   
@endsection

@section('content')



<!--begin::Portlet-->
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Song Edit
                    </h3>
                </div>
            </div>
        </div>

        

        <!--begin::Form-->
        <form method="POST" action="{{route('songs.update', $song->id)}}" class="m-form m-form--fit m-form--label-align-right">
            @csrf
            @method('PUT')
            <div class="m-portlet__body">
                @if ($errors->any())
                    <div class="form-group m-form__group m--margin-top-10">   
                        <div class="alert alert-danger m-alert m-alert--default" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                        </div>
                    </div>
                @endif
                <div class="form-group m-form__group">
                    <label>Title</label>
                    <input type="text" value="{{$song->title}}" name="title" class="form-control m-input m-input--air" placeholder="Enter title">
                </div>
                <div class="form-group m-form__group">
                    <label for="exampleSelect1">Select Category</label>
                    <select name="song_category_id" class="form-control m-input m-input--air">
                        @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach    
                    </select>
                </div>
                <div class="form-group m-form__group">
                    <label>Composer</label>
                    <input type="text" value="{{$song->composer}}" name="composer" class="form-control m-input m-input--air" placeholder="Enter composer">
                </div>
                <div class="form-group m-form__group">
                    <label>Singer</label>
                    <input type="text" name="singer" value="{{$song->singer}}" class="form-control m-input m-input--air" placeholder="Enter singer">
                </div>
                <div class="form-group m-form__group">
                    <label>Key</label>
                    <input type="text" name="key" value="{{$song->key}}" class="form-control m-input m-input--air" placeholder="Enter key">
                </div>
                <div class="form-group m-form__group">
                    <label>Style</label>
                    <input type="text" name="style" value="{{$song->style}}" class="form-control m-input m-input--air" placeholder="Enter style">
                </div>
                <div class="form-group m-form__group">
                    <label>Youtube Id</label>
                    <input type="text" name="youtube_id" value="{{$song->youtube_id}}" class="form-control m-input m-input--air" placeholder="Enter Youtube Id">
                </div>
                <div class="form-group m-form__group">
                    <label for="exampleTextarea">Description</label>
                    <textarea name="description" class="form-control m-input" rows="3">{{$song->description}}</textarea>
                </div>
                <div class="form-group m-form__group">
                    <label>Content</label>
                    <div>
                        <textarea name="content" id="content_editor" class="form-control">{{$song->content}}</textarea>
                    </div>
                </div>
                <div class="form-group m-form__group">
                    <label>Chord</label>
                    <div>
                        <textarea name="chord" id="chord_editor" class="form-control">{{$song->chord}}</textarea>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <button type="submit" class="btn btn-danger">Update</button>
                    <a href="{{route('songs.index')}}" class="btn btn-secondary">Cancel</a>
                </div>
            </div>
        </form>

        <!--end::Form-->
    </div>

    <!--end::Portlet-->
@endsection

@section('head')
    <styLe>
        p {
            /* margin: 5px; */
            line-height: 5;
        }
    </style>
@endsection

@section('script')
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'content_editor' );
        CKEDITOR.replace( 'chord_editor' );
    </script>
@endsection