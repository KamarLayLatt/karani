@extends('layouts.backend')
@section('title','Admin | Welcome')

@section('head_title','Welcome')

@section('breadcrumbs')
    <li class="m-nav__item">
        <a href="{{route('admins.index')}}" class="m-nav__link">
            <span class="m-nav__link-text">Welcome</span>
        </a>
    </li>
@endsection

@section('content')
<div style="padding-left:30px;">
    <p> <h1>Welcome</h1> </p>
</div>
@endsection