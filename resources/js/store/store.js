import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export default new Vuex.Store({

 state: {
  base_url: 'https://karani.org',
  // base_url : 'https://karani.test',
   song_id: '',
   search_lists: '',
   search_data_start: 0,
    song_list: {
		song_lists: '',  
        nextpage: '',
        current_page: 1,
        last_page: 0,
        from: 0,
        menu_no: '',
    },
    login_status: false,
    loader: false,
    register_dialog: false,
    login_dialog: false,

    is_login: false,
 },




 mutations: {
    // GET_SONG_ID(state, data)
    // {
    //   //debugger;
    //   state.song_id = data;
    // },

    // GET_SONG_LIST(state, data)
    // {
    //     state.loader = true;       
    //     axios.get(state.base_url + '/api/app/song/'+ data +'/list?page=1')
    //     .then(response=>{            
    //         state.song_list.song_lists =response.data.data;
    //         state.song_list.current_page = response.data.meta.current_page;
    //         state.song_list.last_page = response.data.meta.last_page;
    //         state.song_list.from = response.data.meta.from; 
    //         state.loader = false;
    //     });
    //     state.song_list.menu_no = data;
    // },

    // GET_SONG_LIST_PAGINATION(state, data)
    // { 
    //     state.loader = true;
    //     axios.get(state.base_url + '/api/app/song/'+ data +'/list?page=' + state.song_list.current_page)
    //     .then(response=>{
    //         state.song_list.song_lists =response.data.data;
    //         state.song_list.current_page = response.data.meta.current_page;
    //         state.song_list.last_page = response.data.meta.last_page;
    //         state.song_list.from = response.data.meta.from; 
    //         state.loader = false;
    //     });
    //     state.song_list.menu_no = data;
    // },
    
    GET_SEARCH_LIST(state, data)
    {
      state.search_data_start = 1;
      state.loader = true;		
      axios.post(state.base_url + '/api/app/song/search', data)
      .then(response=>{
        state.search_lists = response.data.data;
        console.log(response.data.data);
        state.loader = false;
      });
    },

    STOP_SEARCH_LIST(state)
    {
      state.search_data_start = 0;
    },

    GET_LOGIN_DIALOG(state, data)
    {
      state.login_dialog = data;
    },

    GET_REGISTER_DIALOG(state, data)
    {
      state.register_dialog = data;
    },

    GET_LOGIN_STATUS(state, data)
    {
      state.login_status = data;
    }

  },

 actions: {
  
    // get_song_id({commit}, data){
    //     commit('GET_SONG_ID', data)
    //   },
    
    // get_song_list({commit}, data)
    // {
    //   commit('GET_SONG_LIST', data)
    // },

    // get_song_list_pagination({commit}, data)
    // {
    //     commit('GET_SONG_LIST_PAGINATION', data)
	  // },
	
    get_search_list({commit}, data)
    {
      commit('GET_SEARCH_LIST', data);
    },
	
    stop_search_list({commit})
    {
      commit('STOP_SEARCH_LIST');
    },

    get_login_dialog({commit}, data)
    {
      commit('GET_LOGIN_DIALOG', data);
    },

    get_register_dialog({commit}, data)
    {
      commit('GET_REGISTER_DIALOG', data);
    },

    get_login_status({commit}, data)
    {
      commit('GET_LOGIN_STATUS', data);
    }

    

  },

  getters: {
      base_url: state => state.base_url,
      // song_id: state => state.song_id,
      // song_list: state => state.song_list,
      search_lists: state => state.search_lists,
      search_data_start: state => state.search_data_start,
      loader: state => state.loader,
      register_dialog: state => state.register_dialog,
      login_dialog: state => state.login_dialog,
      login_status: state => state.login_status,
      is_login: state => state.is_login,
    }
})