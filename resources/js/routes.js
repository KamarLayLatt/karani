import Vue from 'vue';
import VueRouter from 'vue-router';



Vue.use(VueRouter);

import SongCategoryList from './components/SongCategoryListComponent.vue';
import SongList from './components/SongListComponent.vue';
import SongDetail  from './components/SongDetailComponent.vue';
import SearchList  from './components/SearchListComponent.vue';
import Home  from './components/HomeComponent.vue';
import MyProfile from './components/ProfileComponent.vue';
import CreateSong from './components/my_profile/CreateSongComponent.vue';
import EditSong from './components/my_profile/EditSongComponent.vue';
import Menu from './components/MenuComponent.vue';

const router =new VueRouter({
    base: 'app',
    mode: 'history',
    routes:[
        {
            path: '/',
            name: 'index',
            meta:{
                menu: 'main'
              },
            component: Menu,
            children: [
              {
                path: '/',
                name: 'home',
                meta:{
                  menu: 'main'
                },
                component: Home
              },
              {
                path: '/song/list',
                name: 'songs.list',
                meta:{
                    menu: 'main'
                  },
                component: SongList
              }, 
              {
                  path: '/song/category/:id/list',
                  name: 'songs.categorylist',
                  meta:{
                      menu: 'main'
                    },
                  component: SongCategoryList
              },
              {
                  path: '/songs/search',
                  name: 'songs.search',
                  meta:{
                      menu: 'search'
                    },
                  component: SearchList
              },
            ]
		},
		{
			path: '/songs/:id/show',
			name: 'songs.show',
			meta:{
				menu: 'detail',
				title: 'Song',
			  },
			component: SongDetail
		},
		{
			path: '/my-profile',
			name: 'my-profile',
			meta:{
				menu: 'detail',
				title: 'Profile',
			  },
			component: MyProfile
		},
		{
			path: '/my-profile/create-song',
			name: 'create-song',
			meta:{
				menu: 'detail',
				title: 'Create Song',
			  },
			component: CreateSong
		},
		{
			path: '/my-profile/edit-song/:id',
			name: 'edit-song',
			meta:{
				menu: 'detail',
				title: 'Edit Song',
			  },
			component: EditSong
		},
        
    ]
});

export default router;