
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue';
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Routes from './routes.js'
import App from './App.vue'
import store from './store/store.js'
import fullscreen from 'vue-fullscreen';
import VueZawUni from 'vue-zawuni';
import CKEditor from '@ckeditor/ckeditor5-vue';
import '@mdi/font/css/materialdesignicons.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueYoutube from 'vue-youtube'
var SocialSharing = require('vue-social-sharing');
import VueSocialauth from 'vue-social-auth';
Vue.use(VueAxios, axios)

Vue.use(VueYoutube)
Vue.use(VueSocialauth, {
  providers: {
    facebook: {
      clientId: '589728545097607',
      // redirectUri: 'https://karani.test/app/auth/facebook/callback'
      redirectUri: 'https://karani.org/app/auth/facebook/callback'
    },
    google: {
      clientId: '769444075372-u2lhijbqqjdjfhs4at2ofus9avi9vfo5.apps.googleusercontent.com',
      // redirectUri: 'https://karani.test/app/auth/google/callback'
      redirectUri: 'https://karani.org/app/auth/google/callback'
    },
  }
})
 
Vue.use(SocialSharing);

Vue.use(fullscreen)
Vue.use(VueZawUni)
Vue.use( CKEditor );

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));



Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('app', require('./App.vue').default);
Vue.component('songlist-component', require('./components/SongListComponent.vue').default);
Vue.component('register-component', require('./components/RegisterComponent.vue').default);
Vue.component('login-component', require('./components/LoginComponent.vue').default);
Vue.component('mysong-component', require('./components/my_profile/MySongComponent.vue').default);
Vue.component('favorite-component', require('./components/my_profile/FavoriteComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const opts = { }
Vue.use(Vuetify)

export default new Vuetify({
    icons: {
      iconfont: 'mdi', // default - only for display purposes
    },
  })

const app = new Vue({
    vuetify: new Vuetify(opts),
    el: '#app',
    router: Routes,
    components: { App },
    store,
    fullscreen,
    VueZawUni,
    CKEditor,


});

// export default app;
